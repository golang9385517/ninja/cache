package cache

type Cache interface {
	Set(key string, value interface{})
	Get(key string) interface{}
	Delete(key string)
}

type Storage struct {
	cache map[string]interface{}
}

func New() Storage {
	return Storage{make(map[string]interface{})}
}

func (c Storage) Set(key string, value interface{}) {
	c.cache[key] = value
}

func (c Storage) Get(key string) interface{} {
	return c.cache[key]
}

func (c Storage) Delete(key string) {
	delete(c.cache, key)
}
