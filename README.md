# im-memery-cache 



## Example

```
func main() {
	cache := cache.New()

	cache.Set("userId", 12)
	userId := cache.Get("userId")

	fmt.Println(userId)

	cache.Delete("userId")
	userId = cache.Get("userId")

	fmt.Println(userId)
}
```
## Output

```
    12
    <nil>
```